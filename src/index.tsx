import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import CourseList from './Pages/CourseList';
import CourseAddEdit from './Pages/CourseAddEdit';
import LessonList from './Pages/LessonList';
import LessonAddEdit from './Pages/LessonAddEdit';
import WordList from './Pages/WordList';
import WordAddEdit from './Pages/WordAddEdit';
import Login from './Pages/Login';
import ProtectedRoute from './ProtectedRoute';

const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement
);

root.render(
  <React.StrictMode>
    <BrowserRouter>
    <Routes>
      <Route path='/' element={<App />}>
        {/* Course */}
        <Route path='/courses' element={<ProtectedRoute> 
                                            <CourseList /> 
                                        </ProtectedRoute>} />
        <Route path='/new-course' element={<ProtectedRoute> 
                                            <CourseAddEdit /> 
                                        </ProtectedRoute>}/> 
        <Route path='/update-course/:id' element={<ProtectedRoute> 
                                            <CourseAddEdit /> 
                                        </ProtectedRoute>}/>         
        {/* Lesson */}
        <Route path='/lessons' element={<ProtectedRoute> 
                                            <LessonList /> 
                                        </ProtectedRoute>} />
        <Route path='/new-lesson' element={<ProtectedRoute> 
                                            <LessonAddEdit /> 
                                        </ProtectedRoute>} />
        <Route path='/update-lesson/:id' element={<ProtectedRoute> 
                                            <LessonAddEdit /> 
                                        </ProtectedRoute>}/>    
        {/* Word */}
        <Route path='/words' element={<ProtectedRoute> 
                                            <WordList /> 
                                        </ProtectedRoute>} />
        <Route path='/new-word' element={<ProtectedRoute> 
                                            <WordAddEdit /> 
                                        </ProtectedRoute>}/>
        <Route path='/update-word/:id' element={<ProtectedRoute> 
                                            <WordAddEdit /> 
                                        </ProtectedRoute>}/>   
        {/* Auth */}
        <Route path='/login' element={<Login />} />
      </Route>
    </Routes>
    </BrowserRouter>
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();

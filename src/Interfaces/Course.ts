export default interface Course {
    id: number,
    name: string,
    description: string
}

export default interface Lesson {
    id: number,
    name: string,
    meaning: string,
    pictureUrl: string,
    courseId: number
}

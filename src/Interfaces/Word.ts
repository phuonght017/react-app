export default interface Word {
        id: number,
        wordText: string,
        wordMeaning: string,
        sentence: string,
        wordInSentence: string,
        sentenceMeaning: string,
        audioUrl: string,
        pictureUrl: string,
        lessonId: number,
}

const port = 'http://localhost:3000'
export const loginUrl = `${port}/auth/login`
export const getCoursesUrl = `${port}/admin/courses`
export const getLessonsUrl = `${port}/admin/lessons`
export const getWordsUrl = `${port}/admin/words`
export const createCourseUrl = `${port}/admin/courses/new`
export const createLessonUrl = `${port}/admin/lessons/new`
export const createWordUrl = `${port}/admin/words/new`
export const updateCourseUrl = `${port}/admin/courses/update-course`
export const updateLessonUrl = `${port}/admin/lessons/update-lesson`
export const updateWordUrl = `${port}/admin/words/update-word`
export const getCourseUrl = (id:string|undefined) => {
  return `${port}/admin/courses/${id}`
}  
export const getLessonUrl = (id:string|undefined) => {
  return `${port}/admin/lessons/${id}`
} 
export const getWordUrl = (id:string|undefined) => {
  return `${port}/admin/words/${id}`
} 

import { Link, useNavigate, useParams, useLocation } from 'react-router-dom'
import { useState, useEffect } from 'react'
import '../App.css'
import type { FormEvent } from 'react'
import { createLessonUrl, getCoursesUrl, getLessonUrl, updateLessonUrl } from '../UrlAPI'
import Course from '../Interfaces/Course'
import Lesson from '../Interfaces/Lesson'

const LessonAddEdit = () => {
    const token = sessionStorage.getItem('token');
    const params = useParams<{id: string}>();
    const id = params.id?.substring(1);
    const isAddMode = !id;

    const navigate = useNavigate();
    // Get error from useLocation
    interface propState { error : string }
    const location = useLocation();
    if (location.state) {
        var { error } = location.state as propState;
    } else {
        var error = '';
    }
    // Current detail of lesson
    const [lesson, setLesson] = useState<Lesson>({
        id: 0,
        name: '',
        meaning: '',
        pictureUrl: '',
        courseId: 0
    }); 
    // All Courses
    const [courses, setCourses] = useState<Course[]>([]); 


    useEffect(() => {
        if (!isAddMode) fetchLesson();
        fetchCourses();
     },[]);

    const fetchCourses = async () => {
        const data = await fetch (getCoursesUrl,{
                headers: {
                    'Access-Token':`${token}`
                }
            }
        );

        const object = await data.json();
        console.log(object.courses);
        let courses = object.courses;            
        setCourses(courses);
    }    

    const fetchLesson = async () => {
        const data = await fetch (getLessonUrl(id),{
                headers: {
                    'Access-Token':`${token}`
                }
            }
        );        
        const object = await data.json();
        console.log(object.lesson);
        const thislesson = object.lesson;
        setLesson(thislesson);  
    }

    // Send form to update or create lesson
    const sendForm = async (e: FormEvent<HTMLFormElement>) => {
        e.preventDefault();
                
        // fetch
        if (!isAddMode) {
            fetchUpdate(e)
        } else {
            fetchCreate(e)
        }
    }    

    const fetchUpdate = async (e: FormEvent<HTMLFormElement>) => {
        // get data from form
        const {name, meaning, pictureUrl, courseId} = e.target as typeof e.target & {
            name: {value: string}
            meaning: {value: string}
            pictureUrl: {value: string}
            courseId: {value: number}
        };   

        // check value for request body
        console.log(name.value);
        console.log(meaning.value); 
        console.log(pictureUrl.value); 
        console.log(courseId.value); 

        if (!pictureUrl.value.match(/\.(jpeg|jpg|gif|png)$/)) {
            console.log ('not url');
            navigate(`/update-lesson/:${id}`,{state:{error:'URL is invalid'}});
        } else {
            // post data in JSON 
            await fetch(updateLessonUrl, {
                headers: {
                    'Access-Token':`${token}`,
                    'Content-Type':'application/json'
                },
                method: 'POST',
                body: JSON.stringify({
                    id: id,
                    name: name.value,
                    meaning: meaning.value,
                    pictureUrl: pictureUrl.value,
                    courseId: courseId.value
                })
            })
            navigate('/lessons');
        }        
    }

    const fetchCreate = async (e: FormEvent<HTMLFormElement>) => {
        // get data from form
        const {name, meaning, pictureUrl, courseId} = e.target as typeof e.target & {
            name: {value: string}
            meaning: {value: string}
            pictureUrl: {value: string}
            courseId: {value: number}
        };   

        // check value for request body
        console.log(name.value);
        console.log(meaning.value); 
        console.log(pictureUrl.value); 
        console.log(courseId.value);   

        if (!pictureUrl.value.match(/\.(jpeg|jpg|gif|png)$/)) {
            console.log ('not url');
            navigate(`/new-lesson`,{state:{error:'URL is invalid'}});
        } else {          
            // post data in JSON 
            await fetch(createLessonUrl, {
                headers: {
                    'Access-Token':`${token}`,
                    'Content-Type':'application/json'
                },
                method:'POST',
                body: JSON.stringify({
                    name: name.value,
                    meaning: meaning.value,
                    pictureUrl: pictureUrl.value,
                    courseId: courseId.value
                })
            })  
            navigate('/lessons');   
        }
    }

    return (
        <>  
            <h2>{isAddMode ? 'Create New Lesson' : 'Edit lesson'}</h2>    
            <p className='error'>{error}</p>       
            <form className="wrap" onSubmit={ evt => { sendForm(evt) } }>
                <fieldset>
                    <label htmlFor="name">Name: </label>
                    <input type="text" id="name" defaultValue={lesson.name}></input>
                </fieldset>
                <fieldset>
                    <label htmlFor="meaning">Meaning: </label>
                    <input type="text" id="meaning" defaultValue={lesson.meaning}></input>
                </fieldset>
                <fieldset>
                    <label htmlFor="pictureUrl">Picture (Copy and Paste Url): </label>
                    <input type="text" id="pictureUrl" defaultValue={lesson.pictureUrl}></input>
                </fieldset>
                <fieldset>
                    <label>Course</label>
                    <select name='courseId' id='courseId'>
                        {courses.map(course => (
                            <option value={course.id} selected={(course.id === lesson.courseId)?true:false}>{course.name}</option>
                        ))}
                    </select>
                </fieldset>                   
                <button className='button-new' type="submit">{isAddMode ? 'Create Lesson' : 'Complete Edit'}</button>     
                <Link to='/lessons'><button className='button-new'>Back</button></Link> 
            </form>
        </>
    )
}

export default LessonAddEdit
import {Link} from 'react-router-dom'

const Intro = ({token}:{token:string|null}) => {
    return (
        <>
            <h3> WELCOME TO MY RUSSIAN </h3>
            {(token)?<p>You have successfully logged in. Now you can work with lists of courses, lessons and words by clicking on Navbar</p>:<p>In order to work with data of My Russian you have to log in as an admin!</p>}
            {(!token)?<Link to='/login'><button>Click here to login!</button></Link>:''}
        </>
    )   
}

export default Intro 
import React, { useState, useEffect, useMemo} from "react";
import {Link} from 'react-router-dom'
import '../App.css';
import { getLessonsUrl, getWordsUrl } from "../UrlAPI";
import Word from "../Interfaces/Word";
import Lesson from "../Interfaces/Lesson";

const WordList = () => {
    const token = sessionStorage.getItem('token');
    
    // Reload page after edit
    let data = fetch (
        getWordsUrl,{
            headers: {
                'Access-Token':`${token}`
            }
        }
    );
    useEffect(() => {
        fetchLessons();
        fetchWords();     
    },[data]);

    const [lessons, setLessons] = useState<Lesson[]>([]); // all Lessons
    const [words, setWords] = useState<Word[]>([]); // all Courses
    const [selectedLesson, setSelectedLesson] = useState<number>(0); // Course to apply filter
     
    const fetchLessons = async () => {
        const data = await fetch (
            getLessonsUrl,{
                headers: {
                    'Access-Token':`${token}`
                }
            }
        );

        const object = await data.json();
        console.log(object.lessons);
        let lessons = object.lessons;            
        setLessons(lessons);
    }
    
    const fetchWords = async () => {
        const data = await fetch (
            getWordsUrl,{
                headers: {
                    'Access-Token':`${token}`
                }
            }
        );

        const object = await data.json();
        console.log(object.words);
        let words = object.words;
        setWords(words);        
    }

    // Update selected lesson to filter
    const handleLessonChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
        const value:number = +e.target.value;
        setSelectedLesson(value);
    }

    // Update list of words
    var filteredWords = useMemo(getFilteredList,[selectedLesson,lessons]);
    function getFilteredList() {
        if (!selectedLesson || selectedLesson === 0) {
          return words;
        }
        return words.filter((word) => word.lessonId === selectedLesson);
      }  

    return (
        <div>
            <div className='filter-div'>
                <label>Filter Courses by Lesson</label>
                <select name='lesson-list' id='lesson-list' onChange={handleLessonChange}>
                    <option value='0'>All</option>
                    {lessons.map(lesson => (
                        <option value={lesson.id}>{lesson.name}</option>
                    ))}
                </select>
            </div>

            <table className="course-item">
                <tbody>
                    <tr>
                        <th>Word</th>
                        <th>Meaning</th>
                        <th>Example Sentence</th>
                        <th>Key Word In Sentence</th>
                        <th>Meaning Of Sentence</th>
                        <th>Audio Url</th>
                        <th>Picture</th>
                        <th></th>
                    </tr>
                    {filteredWords.map(word => (
                        <tr key={word.id}>
                            <td>{word.wordText}</td>
                            <td>{word.wordMeaning}</td>
                            <td>{word.sentence}</td>
                            <td>{word.wordInSentence}</td>
                            <td>{word.sentenceMeaning}</td>
                            <td>
                                <audio controls>
                                    <source src={word.audioUrl} type="audio/ogg" />
                                </audio>
                            </td>                        
                            <td><img src={word.pictureUrl} alt={word.wordText} width="70" height="80" /></td>
                            <td><Link to={`/update-word/:${word.id}`}><button className="button-19">Edit</button></Link></td> 
                        </tr>
                    ))}
                </tbody>                
            </table>      
            <Link to='/new-word' ><button className="button-1 course-item">Add New Word</button> </Link>     
        </div>    
    )
}

export default WordList
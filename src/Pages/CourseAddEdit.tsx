import {Link, useNavigate, useParams } from 'react-router-dom';
import { useState, useEffect} from "react";
import '../App.css';
import type { FormEvent } from "react";
import { updateCourseUrl, getCourseUrl, createCourseUrl } from '../UrlAPI';
import Course from '../Interfaces/Course'

const CourseAddEdit = () => {
    const token = sessionStorage.getItem('token');
    const params = useParams<{id: string}>();
    const id = params.id?.substring(1);
    const isAddMode = !id;

    const navigate = useNavigate();
    
    // Get current detail of course
    const [course, setCourse] = useState<Course>({
        id: 0,
        name: '',
        description: '',
    });  
    
    useEffect(() => {
       if (!isAddMode) fetchCourse();
    },[]);

    const fetchCourse = async () => {
        const data = await fetch (
            getCourseUrl(id),{
                headers: {
                    'Access-Token': `${token}`,
                }
            }
        );
        
        const object = await data.json();
        console.log(object.course);
        const thiscourse = object.course;
        setCourse(thiscourse);  
    }

    // Send form to update or create course
    const sendForm = async (e: FormEvent<HTMLFormElement>) => {
        e.preventDefault();
                
        // Post data
        if (!isAddMode) {
            fetchUpdate(e)
        } else {
            fetchCreate(e)
        }

        // redirect to list course after submit
        navigate('/courses');  
    }    

    const fetchUpdate = async (e: FormEvent<HTMLFormElement>) => {
        // get data from form
        const {name, description} = e.target as typeof e.target & {
            name: {value: string}
            description: {value: string}
        };   

        // check value for request body
        console.log(name.value);
        console.log(description.value); 

        // post data in JSON 
        await fetch(updateCourseUrl, {
            headers: {
                'Access-Token':`${token}`,
                'Content-Type':'application/json'
            },
            method:'POST',
            body: JSON.stringify({
                id: id,
                name: name.value,
                description: description.value,
            })
        })
    }

    const fetchCreate = async (e: FormEvent<HTMLFormElement>) => {
        // get data from form
        const {name, description} = e.target as typeof e.target & {
            name: {value: string}
            description: {value: string}
        };   

        // check value for request body
        console.log(name.value);
        console.log(description.value);      
        
        // post data in JSON 
        await fetch(createCourseUrl, {
            headers: {
                'Access-Token':`${token}`,
                'Content-Type':'application/json'
            },
            method:'POST',
            body: JSON.stringify({
                name: name.value,
                description: description.value,
            })
        })         
    }

    return (
        <>            
            <h2>{isAddMode ? 'Create New Course' : 'Edit course'}</h2>           
            <form className="wrap" onSubmit={evt=>{sendForm(evt)}}>
                <fieldset>
                    <label htmlFor="name">Name: </label>
                    <input type="text" id="name" defaultValue={course.name}/>
                </fieldset>
                <fieldset>
                    <label htmlFor="description">Description: </label>
                    <input type="text" id="description" defaultValue={course.description}></input>
                </fieldset>
                <button className='button-new' type="submit">{isAddMode ? 'Create Course' : 'Complete Edit'}</button> 
                <Link to='/courses'><button className='button-new'>Back</button></Link>            
            </form>
        </>
    )
}

export default CourseAddEdit
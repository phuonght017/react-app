import React, { useState, useEffect, useMemo } from "react";
import { Link } from 'react-router-dom'
import '../App.css';
import { getCoursesUrl, getLessonsUrl } from "../UrlAPI";
import Course from "../Interfaces/Course";
import Lesson from "../Interfaces/Lesson";

const LessonList = () => {
    const token = sessionStorage.getItem('token');

    // Reload page after edit
    let data = fetch (
        getLessonsUrl, {
            headers: {
                'Access-Token':`${token}`
            }
        }
    );
    useEffect(() => {
        fetchCourses();
        fetchLessons();     
    }, [data]);

    const [lessons, setLessons] = useState<Lesson[]>([]); // all Lessons
    const [courses, setCourses] = useState<Course[]>([]); // all Courses
    const [selectedCourse, setSelectedCourse] = useState<number>(0); // Course to apply filter
     
    const fetchCourses = async () => {
        const data = await fetch (
            getCoursesUrl,{
                headers: {
                    'Access-Token':`${token}`
                }
            }
        );

        const object = await data.json();
        console.log(object.courses);
        let courses = object.courses;            
        setCourses(courses);
    }
    
    const fetchLessons = async () => {
        const data = await fetch (
            getLessonsUrl,{
                headers: {
                    'Access-Token':`${token}`
                }
            }
        );

        const object = await data.json();
        console.log(object.lessons);
        let lessons = object.lessons;
        setLessons(lessons);        
    }

    // Update selected course to filter
    const handleCourseChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
        const value:number =+ e.target.value;
        setSelectedCourse(value);
    }

    // Update list of lessons
    let filteredLessons = useMemo(getFilteredList,[selectedCourse,lessons]);
    function getFilteredList() {
        if (!selectedCourse || selectedCourse === 0) {
          return lessons;
        }
        return lessons.filter((lesson) => lesson.courseId === selectedCourse);
      }  

    return (
        <div>
            <div className='filter-div'>
                <label>Filter Lessons by Course</label>
                <select name='course-list' id='course-list' onChange={handleCourseChange}>
                    <option value='0'>All</option>
                    {courses.map(course => (
                        <option key={course.id} value={course.id}>{course.name}</option>
                    ))}
                </select>
            </div>

            <table className="course-item">
                <tbody>
                    <tr>
                        <th>Name</th>
                        <th>Meaning</th>
                        <th>Picture</th>
                        <th></th>
                    </tr>
                    {filteredLessons.map(lesson => (
                        <tr key={lesson.id}>
                            <td>{lesson.name}</td>
                            <td>{lesson.meaning}</td>
                            <td><img src={lesson.pictureUrl} alt={  lesson.name} width="70" height="80" /></td>
                            <td><Link to={`/update-lesson/:${lesson.id}`}><button className="button-19">Edit</button></Link></td> 
                        </tr>
                    ))}
                </tbody>
            </table>      
            <Link to='/new-lesson' ><button className="button-1 course-item">Add New Lesson</button> </Link>     
        </div>    
    )
}

export default LessonList
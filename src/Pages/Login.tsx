import { useNavigate, useLocation } from 'react-router-dom'
import '../App.css'
import type { FormEvent } from 'react'
import { loginUrl } from '../UrlAPI'

const Login = () => {
  const navigate = useNavigate()
  // Get error from useLocation
  interface propState { error : string }
  const location = useLocation();
  if (location.state) {
    var { error } = location.state as propState
  } else {
    var error = '';
  }  
  // Send form to update course
    const sendForm = async (e: FormEvent<HTMLFormElement>) => {
        e.preventDefault();
                
        // get data from form
        const {email, password} = e.target as typeof e.target & {
            email: {value: string}
            password: {value: string}
        };       
        
        // post data in JSON 
        console.log(loginUrl);
        let response = await fetch(loginUrl, {
            headers: {
                'Content-Type':'application/json'
            },
            method:'POST',
            body: JSON.stringify({
                email: email.value,
                password: password.value,
            })
        })
        
        if (response.status === 401){
            // Error: Wrong password or email
            navigate('/login',{state:{error:'Your password is not correct'}});
        } else {
            // Save token into session storage
            const object = await response.json();
            let token = object.token;
            sessionStorage.setItem('token',token);
            // redirect to list course after submit
            navigate('/courses'); 
        }    
         
    }    

    return (
        <>             
            <h2>Please Sign In</h2>  
            <div className='error'>
                <p>{error}</p>
            </div>           
            <form className="wrap" onSubmit={evt=>{sendForm(evt)}}>
                <fieldset>
                    <label htmlFor="email">Email: </label>
                    <input type="text" id="email"></input>
                </fieldset>
                <fieldset>
                    <label htmlFor="password">Password: </label>
                    <input type="password" id="password"></input>
                </fieldset>
                <button className='button-new' type="submit">Sign In</button>               
            </form>
            
        </>
    )
}

export default Login
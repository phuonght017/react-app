import {Link, useNavigate, useParams, useLocation } from 'react-router-dom';
import { useState, useEffect} from "react";
import '../App.css';
import type { FormEvent } from "react";
import { createWordUrl, getLessonsUrl, getWordUrl, updateWordUrl } from '../UrlAPI';
import Lesson from '../Interfaces/Lesson';
import Word from '../Interfaces/Word';

const WordAddEdit = () => {
    const token = sessionStorage.getItem('token');
    const params = useParams<{id: string}>();
    const id = params.id?.substring(1);
    const isAddMode = !id;

    const navigate = useNavigate();

    // Get error from useLocation
    interface propState { error : string }
    const location = useLocation();
    if (location.state) {
        var { error } = location.state as propState;
    } else {
        var error = '';
    }
    
    // Current detail of word
    const [word, setWord] = useState<Word>({
        id: 0,
        wordText: '',
        wordMeaning: '',
        sentence: '',
        wordInSentence: '',
        sentenceMeaning: '',
        audioUrl: '',
        pictureUrl: '',
        lessonId: 0,
    });  
    // All Lessons
    const [lessons, setLessons] = useState<Lesson[]>([]); 

    useEffect(() => {
        if (!isAddMode) fetchWord();
        fetchLessons();
     },[]);

    const fetchLessons = async () => {
        const data = await fetch (
            getLessonsUrl,{
                headers: {
                    'Access-Token':`${token}`
                }
            }
        );

        const object = await data.json();
        console.log(object.lessons);
        let lessons = object.lessons;            
        setLessons(lessons);
    }    

    const fetchWord = async () => {
        const data = await fetch (
            getWordUrl(id),{
                headers: {
                    'Access-Token':`${token}`
                }
            }
        );
        
        const object = await data.json();
        console.log(object.word);
        const thisword = object.word;
        setWord(thisword);  
    }

    // Send form to update or create word
    const sendForm = async (e: FormEvent<HTMLFormElement>) => {
        e.preventDefault();
                
        // fetch
        if (!isAddMode) {
            fetchUpdate(e)
        } else {
            fetchCreate(e)
        } 
    }    

    const fetchUpdate = async (e: FormEvent<HTMLFormElement>) => {
        // get data from form
        const {wordText, wordMeaning, sentence, wordInSentence, sentenceMeaning, audioUrl, pictureUrl, lessonId} = e.target as typeof e.target & {
            wordText: {value: string}
            wordMeaning: {value: string}
            sentence: {value: string}
            wordInSentence: {value: string}
            sentenceMeaning: {value: string}
            audioUrl: {value: string}
            pictureUrl: {value: string}
            lessonId: {value: number}
        };   

        // check value for request body
        console.log(wordText.value);
        console.log(wordMeaning.value); 
        console.log(sentence.value); 
        console.log(wordInSentence.value); 
        console.log(sentenceMeaning.value); 
        console.log(audioUrl.value); 
        console.log(pictureUrl.value)
        console.log(lessonId.value)

        if ((!pictureUrl.value.match(/\.(jpeg|jpg|gif|png)$/)) 
        || (!audioUrl.value.match(/\.(mp3|mp4)$/))) {
            console.log ('not url');
            navigate(`/update-word/:${id}`,{state:{error:'URL is invalid'}});
        } else {
            // post data in JSON 
            await fetch(updateWordUrl, {
                headers: {
                    'Access-Token':`${token}`,
                    'Content-Type':'application/json'
                },
                method:'POST',
                body: JSON.stringify({
                    id: id,
                    wordText: wordText.value,
                    wordMeaning: wordMeaning.value,
                    sentence: sentence.value,
                    wordInSentence: wordInSentence.value,
                    sentenceMeaning: sentenceMeaning.value,
                    audioUrl: audioUrl.value,
                    pictureUrl: pictureUrl.value,
                    lessonId: lessonId.value
                })
            })
            // redirect to list of words after submit
            navigate('/words'); 
        }
    }

    const fetchCreate = async (e: FormEvent<HTMLFormElement>) => {
        // get data from form
        const {wordText, wordMeaning, sentence, wordInSentence, sentenceMeaning, audioUrl, pictureUrl, lessonId} = e.target as typeof e.target & {
            wordText: {value: string}
            wordMeaning: {value: string}
            sentence: {value: string}
            wordInSentence: {value: string}
            sentenceMeaning: {value: string}
            audioUrl: {value: string}
            pictureUrl: {value: string}
            lessonId: {value: number}
        };   

        // check value for request body
        console.log(wordText.value);
        console.log(wordMeaning.value); 
        console.log(sentence.value); 
        console.log(wordInSentence.value); 
        console.log(sentenceMeaning.value); 
        console.log(audioUrl.value); 
        console.log(pictureUrl.value)
        console.log(lessonId.value)     
        
        if ((!pictureUrl.value.match(/\.(jpeg|jpg|gif|png)$/)) 
        || (!audioUrl.value.match(/\.(mp3|mp4)$/))) {
            console.log ('not url');
            navigate(`/new-word`,{state:{error:'URL is invalid'}});
        } else {
             // post data in JSON 
            await fetch(createWordUrl, {
                headers: {
                    'Access-Token':`${token}`,
                    'Content-Type':'application/json'
                },
                method:'POST',
                body: JSON.stringify({
                    wordText: wordText.value,
                    wordMeaning: wordMeaning.value,
                    sentence: sentence.value,
                    wordInSentence: wordInSentence.value,
                    sentenceMeaning: sentenceMeaning.value,
                    audioUrl: audioUrl.value,
                    pictureUrl: pictureUrl.value,
                    lessonId: lessonId.value
                })
            })
            // redirect to list of words after submit
            navigate('/words');     
        }        
    }

    return (
        <>              
            <h2>{isAddMode ? 'Create New Word' : 'Edit Word'}</h2>       
            <p className='error'>{error}</p>    
            <form className="wrap" onSubmit={evt=>{sendForm(evt)}}>
                <fieldset>
                    <label htmlFor="wordText">Word: </label>
                    <input type="text" id="wordText" defaultValue={word.wordText}></input>
                </fieldset>
                <fieldset>
                    <label htmlFor="wordMeaning">Meaning: </label>
                    <input type="text" id="wordMeaning" defaultValue={word.wordMeaning}></input>
                </fieldset>
                <fieldset>
                    <label htmlFor="sentence">Sentence: </label>
                    <input type="text" id="sentence" defaultValue={word.sentence}></input>
                </fieldset>
                <fieldset>
                    <label htmlFor="wordInSentence">Key Word In Sentence: </label>
                    <input type="text" id="wordInSentence" defaultValue={word.wordInSentence}></input>
                </fieldset>
                <fieldset>
                    <label htmlFor="sentenceMeaning">Sentence Meaning: </label>
                    <input type="text" id="sentenceMeaning" defaultValue={word.sentenceMeaning}></input>
                </fieldset>
                <fieldset>
                    <label htmlFor="audioUrl">Audio (Copy and Paste Url): </label>
                    <input type="text" id="audioUrl" defaultValue={word.audioUrl}></input>
                </fieldset>
                <fieldset>
                    <label htmlFor="pictureUrl">Picture (Copy and Paste Url): </label>
                    <input type="text" id="pictureUrl" defaultValue={word.pictureUrl}></input>
                </fieldset>
                <fieldset>
                    <label>Course</label>
                    <select name='lessonId' id='lessonId'>
                        {lessons.map(lesson => (
                            <option value={lesson.id} selected={(lesson.id == word.lessonId)?true:false}>{lesson.name}</option>
                        ))}
                    </select>
                </fieldset>    
                <button className='button-new' type="submit">{isAddMode ? 'Create Word' : 'Complete Edit'}</button>                
                <Link to='/words'><button className='button-new'>Back</button> </Link>                    
            </form>
        </>
    )
}

export default WordAddEdit
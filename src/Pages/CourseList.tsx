import { useState, useEffect} from "react";
import {Link, useNavigate} from 'react-router-dom'
import '../App.css';
import { getCoursesUrl } from "../UrlAPI";
import Course from "../Interfaces/Course"

const CourseList= () => {
    const token = sessionStorage.getItem('token');
    const navigate = useNavigate();    

    // Reload page after edit
    let data = fetch (
        getCoursesUrl,{
            headers: {
                'Access-Token':`${token}`
            }
        }
    )
    
    useEffect(() => {
        fetchCourses();
    },[data]);

    const [courses, setCourses] = useState<Course[]>([]);

    const fetchCourses = async () => {
        const response = await fetch (
            getCoursesUrl,{
                headers: {
                    'Access-Token':`${token}`
                }
            }
        );

        
        if (response.status === 403){
            // Error: ROLE is not ADMIN
            sessionStorage.removeItem('token');
            navigate('/login',{state:{error:'You are not an admin. Please use your admin account'}});
        } else {
            // get data to courses
            const data = await response.json();
            console.log(data.courses);
            const courses = data.courses;
            setCourses(courses);
        }         
    }

    return (
        <>
            <h2> Courses </h2>
            <table className="course-item">
                <tbody>
                    <tr>
                        <th>Name</th>
                        <th>Description</th>
                        <th></th>
                    </tr>
                    {courses.map(course => (
                        <tr key={course.id}>
                            <td>{course.name}</td>
                            <td>{course.description}</td>
                            <td><Link to={`/update-course/:${course.id}`}><button className="button-19">Edit</button></Link></td>
                        </tr>
                    ))}
                </tbody>               
            </table>      
            <Link to='/new-course' ><button className="button-1 course-item">Add New Course</button></Link>      
        </>  
    )
}

export default CourseList
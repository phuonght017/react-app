import React from 'react';
import './App.css';
import {Link, Outlet, useNavigate, useLocation} from 'react-router-dom'
import Intro from './Pages/Intro'

function App() {
  const token = sessionStorage.getItem('token'); 
  const navigate = useNavigate();
  const location = useLocation();
  const path = location.pathname;

  const handleLogout = (e: React.MouseEvent<HTMLButtonElement>) => {
    sessionStorage.removeItem('token');
    navigate('/');
  }

  return (
    <div className="App">
      <nav>
        <h3>My Russian</h3>
        <div className='nav-links'>
          <Link to='/courses'>Course</Link>
          <Link to='/lessons'>Lesson</Link>
          <Link to='/words'>Word</Link>
          {(token)?<button className='btn-log' onClick={handleLogout}>Logout</button>:''}
        </div>
      </nav>
      {(path==='/')?<Intro token={token}/>:''}            
      <Outlet />
    </div>
  );
}

export default App;
